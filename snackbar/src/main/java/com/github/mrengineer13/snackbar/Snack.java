/*
 *    Copyright 2014 MrEngineer13
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.github.mrengineer13.snackbar;

//import android.content.res.ColorStateList;
import ohos.agp.utils.Color;
//import android.graphics.Typeface;
import ohos.agp.text.Font;
//import android.os.Parcel;
import ohos.utils.Parcel;
import ohos.rpc.MessageParcel;
//import android.os.Parcelable;
import ohos.utils.Sequenceable;

//class Snack implements Parcelable {
class Snack implements Sequenceable {

    final String mMessage;

    final String mActionMessage;

    final int mActionIcon;

    //    final Parcelable mToken;
    final Sequenceable mToken;

    final short mDuration;

    //    final ColorStateList mBtnTextColor;
    final int mBtnTextColor;

    //    final ColorStateList mBackgroundColor;
    final int mBackgroundColor;

    final int mHeight;

    //    Typeface mTypeface;
    Font mTypeface;

    //    Snack(String message, String actionMessage, int actionIcon,
//                 Parcelable token, short duration, ColorStateList textColor,
//                 ColorStateList backgroundColor, int height, Typeface typeFace) {
    public Snack(String message, String actionMessage, int actionIcon,
                 Sequenceable token, short duration, int textColor, int backgroundColor, int height, Font typeFace) {
        mMessage = message;
        mActionMessage = actionMessage;
        mActionIcon = actionIcon;
        mToken = token;
        mDuration = duration;
        mBtnTextColor = textColor;
        mBackgroundColor = backgroundColor;
        mHeight = height;
        mTypeface = typeFace;
    }
    // reads data from parcel
    public Snack(Parcel p) {
        mMessage = p.readString();
        mActionMessage = p.readString();
        mActionIcon = p.readInt();
//        mToken = p.readParcelable(p.getClass().getClassLoader());
        mToken = p.readSequenceableList(Sequenceable.class).get(0);
        mDuration = (short) p.readInt();
//        mBtnTextColor = p.readParcelable(p.getClass().getClassLoader());
        mBtnTextColor = p.readInt();
//        mBackgroundColor = p.readParcelable(p.getClass().getClassLoader());
        mBackgroundColor = p.readInt();
        mHeight = p.readInt();
//        mTypeface = (Font) p.readValue(p.getClass().getClassLoader());
        mTypeface = (Font) p.readValue();
    }

    // writes data to parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mMessage);
        out.writeString(mActionMessage);
        out.writeInt(mActionIcon);
//        out.writeParcelable(mToken, 0);
        out.writeSequenceable(mToken);
        out.writeInt((int) mDuration);
//        out.writeParcelable(mBtnTextColor, 0);
//        out.writeParcelable(mBackgroundColor, 0);
        out.writeInt(mBtnTextColor);
        out.writeInt(mBackgroundColor);
        out.writeInt(mHeight);
        out.writeValue(mTypeface);
    }

    public int describeContents() {
        return 0;
    }

    // creates snack array
    public static final Sequenceable.Producer<Snack> CREATOR = new Sequenceable.Producer<Snack>() {
        public Snack createFromParcel(Parcel in) {
            return new Snack(in);
        }

        public Snack[] newArray(int size) {
            return new Snack[size];
        }
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
//    public static final Parcelable.Creator<Snack> CREATOR = new Parcelable.Creator<Snack>() {
//        public Snack createFromParcel(Parcel in) {
//            return new Snack(in);
//        }
//
//        public Snack[] newArray(int size) {
//            return new Snack[size];
//        }
//    };
}
