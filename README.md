# SnackBar_ohos

**本项目是基于开源项目SnackBar进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/MrEngineer13/SnackBar ）追踪到原安卓项目版本**

## 项目介绍

- 项目名称：开源SnackBar消息弹框
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：支持显示弹出式提醒，包括文字和点击效果
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目发起作者：蒋筱斌
- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)
- 原项目Doc地址：https://github.com/MrEngineer13/SnackBar

## 项目介绍

- 编程语言：Java

## 安装教程

1. 下载SnackBar jar包snackbar.jar。

2. 启动 DevEco Studio，将下载的jar包，导入工程目录“entry->libs”下。

   ![](./pic1.png)

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

```java
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的jar包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。
5. ![](./pic2.png)
6. ![](./pic3.png)

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1. 对布局进行初始化

```java
ability = this;
context = this;       
com.github.mrengineer13.snackbar.SnackBar.OnMessageClickListener messageClickListener = this;
```

2. 添加snackbar触发按钮

```java
btn.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                   ......
                }
            });
```

3. 添加snackbar消息弹框

```java
mSnackBar = new SnackBar.Builder(ability,component1)
```

4. 自定义弹框信息和样式

```java
.withOnClickListener(messageClickListener)	// 弹框按钮点击效果
.withMessage(message)						// 弹框文本信息
.withActionMessage(btnMessage)				// 弹框按钮文本信息
.withStyle(Color.GREEN.getValue())			// 弹框文本信息颜色设置
.withBackgroundColorId(Color.RED.getValue())// 弹框按钮颜色设置
.withDuration(duration)						// 弹框持续时间设置
.show();									// 显示弹框
```

5. 弹框点击效果设置

```java
@Override
    public void onMessageClick(Sequenceable token) {
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setText("Button clicked!");
        toastDialog.setAlignment(LayoutAlignment.BOTTOM);
        toastDialog.show();
    }
```
## 测试信息

CodeCheck代码测试无异常
VirusTotal病毒安全检测通过
当前版本demo功能与安卓原组件基本无差异
测试员：黎天宁

## 版本迭代

- v0.1.0-alpha

## 版权和许可信息

- SnackBar_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。


