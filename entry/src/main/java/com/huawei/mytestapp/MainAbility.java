/*
 *    Copyright 2014 MrEngineer13
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.huawei.mytestapp;

import com.github.mrengineer13.snackbar.SnackBar;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.PositionLayout;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.utils.Sequenceable;

//import com.huawei.mytestapp.Slice.MainAbilitySlice;

public class MainAbility extends Ability implements SnackBar.OnMessageClickListener {

    private SnackBar mSnackBar;
    private PositionLayout myLayout = new PositionLayout(this);

    private Ability ability;
    private Context context;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(MainAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_activity_snack_bar);
        ability = this;
        context = this;
        com.github.mrengineer13.snackbar.SnackBar.OnMessageClickListener messageClickListener = this;

        Component component1 = findComponentById(ResourceTable.Id_main);

        Button btn = (Button) findComponentById(ResourceTable.Id_btn);
        if (btn != null) {
            btn.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    String message = "This is a new message!";
                    int messageRes = -1;
                    short duration = 2000;
                    int bgColor;
                    String btnMessage = "click";
//                    com.github.mrengineer13.snackbar.SnackBar.Style style = com.github.mrengineer13.snackbar.SnackBar.Style.ALERT;


                        mSnackBar = new SnackBar.Builder(ability,component1)
                                .withOnClickListener(messageClickListener)
                                .withMessage(message)
                                .withActionMessage(btnMessage)
                                .withStyle(Color.GREEN.getValue())
                                .withBackgroundColorId(Color.RED.getValue())
                                .withDuration(duration)
                                .withTypeFace(Font.DEFAULT)
                                .show();
                }
            });
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onMessageClick(Sequenceable token) {
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setText("Button clicked!");
        toastDialog.setAlignment(LayoutAlignment.BOTTOM);
        toastDialog.show();
    }
}
